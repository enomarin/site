export const load = async ({ fetch }) => {
	const response = await fetch(`/api/posts`);
	const posts: (string | object)[] = await response.json();
	/*
	await Promise.all(
		posts.map(async (post, index) => {
			let query = 'w=600&format=webp';
			let path = `./${post.path}/1.jpg`;
			const image = (x) => new URL(path, import.meta.url)
			console.log(image());
			
			posts[index].image = image;
		})
	);
	*/
	return {
		posts
	};
};
