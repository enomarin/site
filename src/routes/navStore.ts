import { writable } from 'svelte/store';
const nav = {
	focus: 1,
	page: 'home'
};

const { subscribe, set, update } = writable(nav);
const focus = (focused) =>
	update((nav) => {
		nav.focus = focused;
		return nav;
	});
const unFocus = () =>
	update((nav) => {
		nav.focus = -1;
		return nav;
	});

const reset = () => {
	set(nav);
};

export default {
	subscribe,
	focus,
	unFocus,
	reset
};
