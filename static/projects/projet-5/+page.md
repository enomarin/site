---
title: Les formes du mouvement
description: Ut sequi autem aut autem impedit et quia sunt et incidunt similique ad neque maxime qui ipsam fugit non quia voluptas.
date: '2022'
categories:
  - art
  - bitcoin
published: true
status: past
---

## Flickering Transactions
Flickering Transactions s’inscrit dans une démarche de recherche sur l’impact des cryptomonnaies sur le réel. Cette démarche s’appuie sur le détournement d’objets électroniques par le biais d’une programmation informatique. Ces objets, animés par des flux de données circulant sur le réseau Bitcoin, sont habités d’un comportement qui reflète l’organicité d’un réseau partagé entre humains et machines.

Une première partie de l’installation est lumineuse et composée de plusieurs tubes fluorescents contrôlés par des relais électroniques. Elle est de dimension variable et s’adapte au lieu dans lequel elle est exposée. Un nano-ordinateur écoute en temps réel les transactions qui circulent sur le réseau Bitcoin et transcrit ces informations par une extinction de la lumière.

En soutien de l’installation lumineuse, une imprimante thermique est installée dans l’espace d’exposition et imprime les données du réseau Bitcoin en temps réel.

L'installation à fait l'objet d'une résidence de création au Grand Cordel MJC.

### Bonjour
Hello 